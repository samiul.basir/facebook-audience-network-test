package com.sbappstore.banglaKobita;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.*;

public class MainActivity extends AppCompatActivity {

    private AdView adView;
    private InterstitialAd interstitialAd;

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AudienceNetworkAds.initialize(this);

        textView = (TextView) findViewById(R.id.textView);


        interstitialAd = new InterstitialAd(this, getResources().getString(R.string.fb_Interstitial_Ads_Id));
        interstitialAd.setAdListener(new InterstitialAdListener() {
            @Override
            public void onInterstitialDisplayed(Ad ad) {
                // Interstitial ad displayed callback
            }
            @Override
            public void onInterstitialDismissed(Ad ad) {
                // Interstitial dismissed callback
            }
            @Override
            public void onError(Ad ad, AdError adError) {
                Toast.makeText(MainActivity.this, "Failed to load interstital ad", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onAdLoaded(Ad ad) {
                // Interstitial ad is loaded and ready to be displayed
                // Show the ad
                interstitialAd.show();
            }
            @Override
            public void onAdClicked(Ad ad) {
                // Ad clicked callback
            }
            @Override
            public void onLoggingImpression(Ad ad) {
                // Ad impression logged callback
            }
        });
        interstitialAd.loadAd();

        adView = new AdView(this, getResources().getString(R.string.fb_Banner_Ads_Id),
                AdSize.BANNER_HEIGHT_50);
        LinearLayout adContainer = (LinearLayout) findViewById(R.id.banner_container);
        adContainer.addView(adView);
        adView.loadAd();

        adView.setAdListener(new AdListener() {
            @Override
            public void onError(Ad ad, AdError adError) {
                textView.setText("Ad Loading failed, something went wrong!");
                textView.setTextColor(getResources().getColor(R.color.RED));
            }

            @Override
            public void onAdLoaded(Ad ad) {
                textView.setText("Congratulations! Ad Loaded Successfully");
                textView.setTextColor(getResources().getColor(R.color.Green));
            }

            @Override
            public void onAdClicked(Ad ad) {

            }

            @Override
            public void onLoggingImpression(Ad ad) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        if (adView != null) {
            adView.destroy();
        }
        super.onDestroy();
    }
}

